# rs-persistent_rope

An immutable persistent rope data structure

```rust
extern crate persistent_rope;

use persistent_rope::Rope;

fn main() {
    let a: Rope = "Hello".into();
    let b = a + ", world!";
    println!("{:?}", b);
}
```
