#![feature(test)]

extern crate an_rope;
extern crate persistent_rope;
extern crate ropey;
extern crate test;
extern crate xi_rope;

use self::test::Bencher;

#[bench]
fn an_rope(b: &mut Bencher) {
    let rope = an_rope::Rope::from("abc");

    b.iter(|| {
        let new_rope = rope.insert_str(3, "def");
        let new_rope = new_rope.delete(1..5);

        let new_rope = new_rope.insert(1, ' ');
        let _ = new_rope.delete(1..2);
    });
}

#[bench]
fn persistent_rope(b: &mut Bencher) {
    let rope = persistent_rope::Rope::from("abc");

    b.iter(|| {
        let new_rope = rope.insert(3, "def");
        let new_rope = new_rope.remove_range(1..5);

        let new_rope = new_rope.insert_char(1, ' ');
        let _ = new_rope.remove(1);
    });
}

#[bench]
fn ropey(b: &mut Bencher) {
    let rope = ropey::Rope::from_str("abc");

    b.iter(|| {
        let mut new_rope = rope.clone();

        new_rope.insert(3, "def");
        new_rope.remove(1..5);

        new_rope.insert_char(1, ' ');
        new_rope.remove(1..2);
    });
}

#[bench]
fn xi_rope(b: &mut Bencher) {
    let rope = xi_rope::Rope::from("abc");

    b.iter(|| {
        let mut new_rope = rope.clone();

        new_rope.edit(3..3, "def");
        new_rope.edit(1..5, "");

        new_rope.edit(1..1, " ");
        new_rope.edit(1..2, "");
    });
}
